package hw;

import java.util.Arrays;

public class Main {

  public static void main(final String[] args) {
    System.out.println("args = " + Arrays.asList(args));
    final HelloWorld instance = new HelloWorld();
    System.out.println(instance.getMessage()); // prints Hello world on this command.
    System.out.println(instance.getYear()); // prints 2021 on this line of command.
    System.out.println("bye for now"); // prints bye for now.
  }
}
